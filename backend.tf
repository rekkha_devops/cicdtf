terraform {
  backend "s3" {
    bucket = "s3statebucket900"
    key    = "global/mystatefile/terraform.tfstate"
    region = "us-east-1"
    dynamodb_table = "state-lock"
  }
}
