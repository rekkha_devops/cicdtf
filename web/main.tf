resource "aws_instance" "server" {
    ami = "ami-051f8a213df8bc089"
    instance_type = "t2.micro"
    subnet_id = var.sn
    security_groups = [var.sg]
    tags = {
        Name = "myserver"
    }
}